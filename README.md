# Laravel Passport Implementacion Servidor

## Table of Contents

- [About](#about)
- [Installing](#installing)
- [Usage](#usage)

## About <a name="about"></a>

Laravel already makes it easy to perform authentication via traditional login forms, but what about APIs? APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport, which provides a full OAuth2 server implementation for your Laravel application in a matter of minutes. Passport is built on top of the League OAuth2 server that is maintained by Andy Millington and Simon Hamp.

### Installing <a name="installing"></a>

Clonar el repositorio y navegar hasta al interior de la carpeta server para instalar las dependencias.

```
cd server
composer install
```
Luego correr las migraciones

```
php artisan migrate
```
Repetir el mismo procedimiento con la carpeta client.

Asegurarse de crear un virtual host para la aplicacion server (Ej. server.lcl) y para client.



### Usage <a name="usage"></a>

En la aplicación server, navegue hasta la ruta `http://server.lcl/developers`. Y creé nuevo cliente OAuth. Esto generará un client ID y secret.

A continuación, en la aplicación client, agregue las siguientes configuraciones en el archivo .env y estará listo para usar.

```
OAUTH_SERVER_ID=<client-id>
OAUTH_SERVER_SECRET=<client=secret>
OAUTH_SERVER_REDIRECT_URI=http://client.lcl/oauth/callback
OAUTH_SERVER_URI=http://server.lcl
```